1) fetch this project on your machine
2) if your OS Linux run script start (if your OS different from Linux, go to NOTE and do steps)
3) go to the src/main/java/book/theautomatedtester/co/uk/core
4) open for editing file LocalSettings.java
5) set variable url = "http://youwebsite.com" (in our case url from practical task) and save file
6) run command mvn clean test


NOTE: if script start does not work you can do it manually:
1) go to the https://www.seleniumhq.org/download/ and download WebDriver for your OS
2) create directory lib inside project and unzip archive inside directory lib
3) run file LocalSettingsGenerator.jar (java -jar LocalSettingsGenerator.jar)
4) move file LocalSettings.java to the src/main/java/book/theautomatedtester/co/uk/core
5) open for editing file LocalSettings.java
6) set variable url = "http://youwebsite.com" (in our case url from practical task)
7) if you need it, change path to driver (lib/chromdriver) in variable new ConfigDriver("lib/chromedriver", "webdriver.chrome.driver"),
8) run command mvn clean test
