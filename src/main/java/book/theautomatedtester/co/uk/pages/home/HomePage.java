package book.theautomatedtester.co.uk.pages.home;

import book.theautomatedtester.co.uk.core.WebExtension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends WebExtension {

  @FindBy(xpath = "//a[@href='/chapter1']")   private WebElement linkChapter1;


  public HomePage(WebDriver driver) {
    super(driver);
  }

  public void clickOnChapter1(){
    log.info("Wait and click on webelement linkChapter1");
    waitAndClick(linkChapter1);
  }

  public void checkFlagHomePage(){
    log.info("Check flag HomePage");
    waitElement(linkChapter1);
  }

}
