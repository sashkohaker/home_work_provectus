package book.theautomatedtester.co.uk.pages.home.chapter1;

import book.theautomatedtester.co.uk.core.WebExtension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class Chapter1_Page extends WebExtension {

  @FindBy(id = "divontheleft")      private WebElement AssertFlag;
  @FindBy(xpath = "//a[@href='/']") private WebElement linkHomePage;

  public Chapter1_Page(WebDriver driver){
    super(driver);
  }

  public void checkAssertTextOnThePage(){
    log.info("Check assertFlag on the page");
    Assert.assertEquals(AssertFlag.getText(), "Assert that this text is on the page");
  }

  public void clickOnLinkHomePage(){
    log.info("Wait and click on link HomePage");
    waitAndClick(linkHomePage);
  }

}
