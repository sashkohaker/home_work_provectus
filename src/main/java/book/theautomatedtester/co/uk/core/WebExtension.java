package book.theautomatedtester.co.uk.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public abstract class WebExtension {

  private WebDriverWait wait;
  protected WebDriver driver;
  public static Logger log = Logger.getLogger(WebExtension.class.getName());


  public WebExtension(WebDriver driver){
    this.driver = driver;

    wait = new WebDriverWait(driver, 10);

    Properties preferences = new Properties();
    try{
      FileInputStream configFile = new FileInputStream("logging.properties");
      preferences.load(configFile);
      LogManager.getLogManager().readConfiguration();
    }catch(IOException e){
      System.err.println("Could not setup logger configuration: " + e.toString());
    }
  }

  /**
   * This method opens url in web browser.
   *
   * @param url url for opening.
   */
  public void openUrl(String url){
    try{
      log.info("Driver will navigate to " + url);
      driver.navigate().to(url);
      log.info("Driver navigated to " + url);
    }catch(Exception e){e.printStackTrace();}
  }

  /**
   * This method waits till the element will be visible and clickable, after that clicks on element.
   *
   * @param element - webelement
   */
  public void waitAndClick(WebElement element){
    try{
      log.info("Wait till element " + element.toString() + " will be visible");
      wait.until(ExpectedConditions.visibilityOf(element));
      log.info("Wait till element " + element.toString() + " will be clickable");
      wait.until(ExpectedConditions.elementToBeClickable(element));
      log.info("Click on element " + element.toString());
      element.click();
      log.config("Element was clicked " + element.toString());
    }catch(Exception e){
      System.err.println(e.getCause() + "\n" + e.getMessage());
      //e.printStackTrace();
      Assert.assertTrue(element.isEnabled(),"The element was not enabled!");
      Assert.assertTrue(element.isDisplayed(),"The element was not displayed!");
    }
  }

  /**
   * Closing and destroy the current sample of a driver.
   */
  public void exit(){
    log.info("Quit driver");
    try{
      driver.quit();
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  /**
   * Wait webelement
   *
   * @param element WebElement
   */
  public void waitElement(WebElement element){
    try{
      wait.until(ExpectedConditions.visibilityOf(element));
    }catch(Exception e){
      e.printStackTrace();
      Assert.assertTrue(element.isDisplayed());
    }
  }

}
