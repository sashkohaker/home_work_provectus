package book.theautomatedtester.co.uk.core;


import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;


public class Controller {
	private static Controller instance;

	public WebDriver driver;
	public  LocalSettings localSettings = new LocalSettings();


	private Controller(){
		System.setProperty(
			localSettings.driversMap.get(localSettings.getBrowser()).getBrowser(),
			localSettings.driversMap.get(localSettings.getBrowser()).getPathToDriver());
		if(localSettings.getBrowser().equals("chrome")){
			driver = new ChromeDriver();
		}
		if(localSettings.getBrowser().equals("firefox")){
			driver = new FirefoxDriver();
		}
		if(localSettings.getBrowser().equals("opera")){
			driver = new OperaDriver();
		}
		// driver.manage().window().maximize();
		if(driver != null){
			driver.manage().window().setSize(new Dimension(1920,1080));
		}else{ System.err.println("ERROR: WEBDRIVER is NULL!!!"); }
	}

	public static Controller getInstance(){
		if(instance == null){
			instance = new Controller();
		}
		return instance;
	}


}
