package book.theautomatedtester.co.uk.core;
import java.util.HashMap;
import java.util.Map;

public class LocalSettings {
    private String browser = "firefox";
    private String url = "http://book.theautomatedtester.co.uk/";

    public LocalSettings(){
        driversMap.put("chrome", configDriversArray[0]);
        driversMap.put("firefox", configDriversArray[1]);
        driversMap.put("opera", configDriversArray[2]);

    }

    public String getMainUrl(){
        return url;
    }

    public String getBrowser(){
        return browser;
    }


    public Map<String, ConfigDriver> driversMap = new HashMap();

    ConfigDriver[] configDriversArray = new ConfigDriver[]{
            new ConfigDriver("lib/chromedriver", "webdriver.chrome.driver"),
            new ConfigDriver("lib/geckodriver", "webdriver.gecko.driver"),
            new ConfigDriver ("lib/operadriver", "webdriver.opera.driver"),
    };


    class ConfigDriver {
        private String pathToDriver;
        private String browser;
        public ConfigDriver(String pathToDriver, String browser){
            this.pathToDriver = pathToDriver;
            this.browser = browser;
        }

        public String getPathToDriver() {
            return this.pathToDriver;
        }

        public String getBrowser(){
            return this.browser;
        }
    }

}
