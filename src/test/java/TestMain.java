import book.theautomatedtester.co.uk.core.Controller;
import book.theautomatedtester.co.uk.pages.home.HomePage;
import book.theautomatedtester.co.uk.pages.home.chapter1.Chapter1_Page;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestMain {

  public Controller controller = Controller.getInstance();

  protected HomePage homePage           = PageFactory.initElements(controller.driver, HomePage.class);
  protected Chapter1_Page chapter1_Page = PageFactory.initElements(controller.driver, Chapter1_Page.class);

  @BeforeMethod
  public void setUp(){
    homePage.openUrl(controller.localSettings.getMainUrl());
  }

  @AfterSuite
  public void finish(){
    homePage.exit();
  }

  @Test
  public void testCahpter1(){
    homePage.clickOnChapter1();
    chapter1_Page.checkAssertTextOnThePage();
    chapter1_Page.clickOnLinkHomePage();
    homePage.checkFlagHomePage();
  }

}
